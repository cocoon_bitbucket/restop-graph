# coding=utf-8

"""
Uses df to collect data on disk spaces

#### Dependencies

 * df


sample:

alpine:~$ df
Filesystem     1K-blocks   Used Available Use% Mounted on
devtmpfs           10240      0     10240   0% /dev
shm               253504      0    253504   0% /dev/shm
/dev/sda3        7026040 375056   6271032   6% /
tmpfs              50704    120     50584   1% /run
cgroup_root        10240      0     10240   0% /sys/fs/cgroup
/dev/sda1          95054  30245     57641  35% /boot




"""

import collector
import re
import os
import multiprocessing
from collector import Collector,str_to_bool


class DfCollector(Collector):

    PROC_DF = 'df'

    # pattern:  /dev/sda1          95054  30245     57641  35% /boot
    PROC_DF_RE = re.compile('([\S]+)\s+([\d.]+)\s+([\d.]+)\s+([\d.]+)\s+([\d.]+)\%\s+([\S]+)')

    @classmethod
    def get_command_line(cls, **kwargs):
        return "df"


    def get_default_config_help(self):
        config_help = super(DfCollector,
                            self).get_default_config_help()
        config_help.update({
            'simple':   'Only collect'
        })
        return config_help

    def get_default_config(self):
        """
        Returns the default collector settings
        """
        config = super(DfCollector, self).get_default_config()
        config.update({
            'path':     'df',
            'simple':   'False'
        })
        return config


    def normalize_file_system(self,name):
        """

        :param name: string , name of filesystem eg /dev/disk1
        :return:
        """
        if name[0] == '/':
            name=name[1:]
        name= name.replace('/',"_")
        return name

    def collect_metric(self, lines, timestamp):
        """

            parse df metric lines

            /dev/sda1          95054  30245     57641  35% /boot

        :param self:
        :param lines:
        :param timestamp:
        :return:
        """
        labels= ['Filesystem','Size','Used','Avail','UsedPct' ,'Mount']
        if lines:
            first= lines[0]
            for line in lines[1:]:

                match = self.PROC_DF_RE.match(line)
                if match:
                    file_system = self.normalize_file_system(match.group(1))
                    size=         float(match.group(2))
                    used = float(match.group(3))
                    avail = float(match.group(4))
                    used_pct = int(match.group(5))
                    mount = match.group(6)

                    # publish
                    self.publish_gauge("%s.Size" % file_system, size, 2, timestamp=timestamp)
                    self.publish_gauge("%s.Used" % file_system, used, 2, timestamp=timestamp)
                    self.publish_gauge("%s.Avail" % file_system, avail, 2, timestamp=timestamp)
                    self.publish_gauge("%s.UsedPct" % file_system, used_pct, 1, timestamp=timestamp)

                else:
                    pass
        return