# coding=utf-8

"""
Uses /proc/loadavg to collect data on load average

#### Dependencies

 * /proc/loadavg

"""

import collector
import re
import os
import multiprocessing
from collector import Collector,str_to_bool


class LoadAverageCollector(Collector):

    PROC_LOADAVG = '/proc/loadavg'
    PROC_LOADAVG_RE = re.compile(r'([\d.]+) ([\d.]+) ([\d.]+) (\d+)/(\d+)')

    @classmethod
    def get_command_line(cls, **kwargs):
        return "cat /proc/loadavg"


    def get_default_config_help(self):
        config_help = super(LoadAverageCollector,
                            self).get_default_config_help()
        config_help.update({
            'simple':   'Only collect the 1 minute load average'
        })
        return config_help

    def get_default_config(self):
        """
        Returns the default collector settings
        """
        config = super(LoadAverageCollector, self).get_default_config()
        config.update({
            'path':     'loadavg',
            'simple':   'False'
        })
        return config



    def collect_metric(self, lines, timestamp, cpu_count=1):
        """

        :param self:
        :param lines:
        :param timestamp:
        :return:
        """

        if lines:
            for line in lines:
                match = self.PROC_LOADAVG_RE.match(line)
                if match:
                    load01 = float(match.group(1))
                    load05 = float(match.group(2))
                    load15 = float(match.group(3))
                    proc_running = int(match.group(4))
                    proc_total = int(match.group(5))

                    if not str_to_bool(self.config['simple']):
                        self.publish_gauge('01', load01, 2, timestamp=timestamp)
                        self.publish_gauge('05', load05, 2, timestamp=timestamp)
                        self.publish_gauge('15', load15, 2, timestamp=timestamp)
                        self.publish_gauge('01_normalized', load01 / cpu_count, 2, timestamp=timestamp)
                        self.publish_gauge('05_normalized', load05 / cpu_count, 2, timestamp=timestamp)
                        self.publish_gauge('15_normalized', load15 / cpu_count, 2, timestamp=timestamp)
                    else:
                        self.publish_gauge('load', load01, 2, timestamp=timestamp)
                        self.publish_gauge('load_normalized', load01 / cpu_count, 2, timestamp=timestamp)

                    self.publish_gauge('processes_running',proc_running, timestamp=timestamp)
                    self.publish_gauge('processes_total', proc_total, timestamp=timestamp)