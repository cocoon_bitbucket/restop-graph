

write sample in carbon
=======================

    echo "platform-1.tv.1.cpu 2 `date +%s`" | nc localhost 22003



render graph in graphite
=========================
    https://127.0.0.1:8443/render?target=platform-1.tv.1.cpu&height=800&width=600






cpu samples
===========


cpu
---

alpine:~$ cat /proc/stat
cpu  31 0 94 5773088 0 0 0 0 0 0
cpu0 31 0 94 5773088 0 0 0 0 0 0
intr 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
ctxt 0
btime 1466693266
processes 0
procs_running 0
procs_blocked 0
softirq 0 0 0 0 0 0 0 0 0 0 0



alpine:~$ cat /proc/stat | grep cpu
cpu  31 0 94 5774149 0 0 0 0 0 0
cpu0 31 0 94 5774149 0 0 0 0 0 0



df -h
-----

alpine:~$ df -h
Filesystem      Size  Used Avail Use% Mounted on
devtmpfs         10M     0   10M   0% /dev
shm             248M     0  248M   0% /dev/shm
/dev/sda3       6.8G  363M  6.0G   6% /
tmpfs            50M  120K   50M   1% /run
cgroup_root      10M     0   10M   0% /sys/fs/cgroup
/dev/sda1        93M   30M   57M  35% /boot



meminfo
-------

alpine:~$ cat /proc/meminfo
MemTotal:         507012 kB
MemFree:          475340 kB
MemAvailable:     484088 kB
Buffers:            3392 kB
Cached:            11308 kB
SwapCached:            0 kB
Active:            12084 kB
Inactive:           4268 kB
Active(anon):       1696 kB
Inactive(anon):      384 kB
Active(file):      10388 kB
Inactive(file):     3884 kB
Unevictable:           0 kB
Mlocked:               0 kB
SwapTotal:             0 kB
SwapFree:              0 kB
Dirty:                 0 kB
Writeback:             0 kB
AnonPages:          1708 kB
Mapped:             3728 kB
Shmem:               428 kB
Slab:               9180 kB
SReclaimable:       3144 kB
SUnreclaim:         6036 kB
KernelStack:         928 kB
PageTables:          464 kB
NFS_Unstable:          0 kB
Bounce:                0 kB
WritebackTmp:          0 kB
CommitLimit:      253504 kB
Committed_AS:       6792 kB
VmallocTotal:   34359738367 kB
VmallocUsed:        3400 kB
VmallocChunk:   34359685948 kB
DirectMap4k:        8128 kB
DirectMap2M:      516096 kB


loadavg
-------

alpine:~$ cat /proc/loadavg
0.00 0.01 0.02 3/59 1985


uptime
------

alpine:~$ cat /proc/uptime
61316.16 61297.55


