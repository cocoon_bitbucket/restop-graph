"""

    available collectors


"""

from configobj import ConfigObj

from gdiamond.graphite import GraphiteHandler
from gdiamond.collector_cpu import CPUCollector
from gdiamond.collector_loadavg import LoadAverageCollector
from gdiamond._collector_memory import MemoryCollector
from gdiamond.collector_df import DfCollector

import logging
log=logging.getLogger('restop_graph')



catalog= {
    'cpu': CPUCollector,
    'loadavg': LoadAverageCollector,
    'memory': MemoryCollector,
    'df': DfCollector,
}

def get_collector_class(collector_name):
    """

    :param collector_name:
    :return:
    """

    if collector_name not in catalog.keys():
        msg = "collector: [%s] is not avalaible" % collector_name
        log.error(msg)
        raise RuntimeError(msg)
    return catalog[collector_name]


def get_collector_command(collector_name, **kwargs):
    """

    :param collector_name:
    :param kwargs:
    :return:
    """
    collector_class= get_collector_class(collector_name)
    return collector_class.get_command_line(**kwargs)


def get_graphite_collector( collector_name, config=None,configfile='collectors.ini'):
    """

    :param config:
    :param configfile:
    :return:
    """
    config= config or {}
    conf = ConfigObj(configfile)
    conf.update(config)

    handler = GraphiteHandler(config=conf)
    collector_class= get_collector_class(collector_name)
    collector= collector_class(config=conf ,configfile=configfile,handlers=[handler] )

    return collector


