
"""

    tools to produce stats on a busy box

    stats are produced on a destination file eg /tmp/trace

    a stat content a header with a >>> prefix, an iso timestamp and the name of the command
    and a body : the command result



    sample /tmp/trace file:

    \>>> 2016-06-22T15:37:27Z cpu_snapshot
    0.00 0.01 0.01 2/60 1831
    \>>> 2016-06-22T15:39:25Z loadavg
    0.00 0.01 0.01 2/60 1834




    # get date time in full iso format
    date -u +"%Y-%m-%dT%H:%M:%SZ"
    eg: 2016-06-22T14:56:19Z



get status on busybox
---------------------

>    cat /proc/loadavg

    eg: 0.00 0.01 0.01 3/60 1800

>    cat /proc/meminfo

    eg:
        MemTotal:         507012 kB
        MemFree:          398616 kB
        ...

        DirectMap4k:        8128 kB
        DirectMap2M:      516096 kB


>    cat /proc/cpuinfo

    eg: processor	: 0
    vendor_id	: GenuineIntel
    ...
    address sizes	: 39 bits physical, 48 bits virtual
    power management:



>   ps aux | sort -nk +4 | tail

    eg
        1772 vagrant    0:00 sshd: vagrant@pts/0
        1773 vagrant    0:00 -ash
        1774 vagrant    0:00 ash
        1803 vagrant    0:00 ps aux
        1804 vagrant    0:00 sort -nk +4
        1805 vagrant    0:00 [tail]
        PID   USER     TIME   COMMAND


>  df -H

    eg
        Filesystem      Size  Used Avail Use% Mounted on
        devtmpfs         11M     0   11M   0% /dev
        shm             260M     0  260M   0% /dev/shm
        /dev/sda3       7.2G  383M  6.5G   6% /
        tmpfs            52M  123k   52M   1% /run
        cgroup_root      11M     0   11M   0% /sys/fs/cgroup
        /dev/sda1        98M   31M   60M  35% /boot


> free -m

    eg

                     total       used       free     shared    buffers     cached
        Mem:           495        105        389          0          4         78
        -/+ buffers/cache:         22        472
        Swap:            0          0          0


"""
import time
import subprocess
import heapq
import datetime

from restop_graph import catalog

import logging

log=logging.getLogger(__name__)


iso_date_format= 'date -u +"%Y-%m-%dT%H:%M:%SZ"'    #':%3N'


class TimestampConverter(object):
    """



    """

    @classmethod
    def iso_to_epoq(cls, timestamp):
        """
            convert timestamp to epoq

        :param timestamp:
        :return:
        """
        #  '2016-08-10T10:57:58Z' does not match format '%Y%m%dT%H%M%S.%fZ'

        t = datetime.datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%SZ")
        timestamp = t.strftime('%s')
        return timestamp


    @classmethod
    def epoq_to_datetime(cls,epoq):
        """



        :param epoq:
        :return:
        """
        timestamp= float(epoq)
        dt = datetime.datetime.fromtimestamp(timestamp)
        return dt


class TraceDriver(object):
    """

        generate commands to produce stats ( mem , cpu ... )


    """
    def __init__(self,filename='/tmp/trace',**kwargs):
        """

        :param filename:
        """
        self.filename=filename
        self.setup(**kwargs)


    def setup(self,**kwargs):
        """

        :param kwargs:
        :return:
        """
        self.parameters= kwargs

    @classmethod
    def get_collector_command(cls,collector_name):
        """

        :param collector_name: eg
        :return:
        """
        return catalog.get_collector_command(collector_name)



    def gen_command_lines(self,name,command=None):
        """

            generate command lines with iso date prefix


            eg:
                echo ">>>" `date -u +"%Y-%m-%dT%H:%M:%SZ"` loadavg >> /tmp/trace
                cat /proc/loadavg >> /tmp/trace

        :param name: string name of the command
        :param filename: string, name of the destination file
        :param command: string, body of the shell command
        :return: list of 2 commands
        """
        lines= []

        header = 'echo ">->" `date -u +"%Y-%m-%dT%H:%M:%SZ"` ' + name + " >> " + self.filename
        body   = command + " >> " + self.filename
        footer = 'echo ">-<" >> %s'% self.filename

        lines.append(header)
        lines.append(body)
        lines.append(footer)

        return lines


    def gen_start_trace(self):
        """

        :param filename:
        :return:
        """
        lines = []
        lines.append('rm %s' % self.filename)
        lines.append('echo ">--" `date -u +"%Y-%m-%dT%H:%M:%SZ"` start_trace > ' + self.filename)
        return lines

    def gen_remove_trace(self):
        """

        :param filename:
        :return:
        """
        lines = []
        lines.append('rm %s' % self.filename)
        return lines


    def gen_dump_command(self):
        """


        :param filename:
        :return:
        """
        lines = []

        lines.append('echo ">-[" `date -u +"%Y-%m-%dT%H:%M:%SZ"` dump_trace')
        lines.append('cat %s' % self.filename)
        lines.append('echo ">-]" `date -u +"%Y-%m-%dT%H:%M:%SZ"` dump_trace')

        return lines


    def run_local(self,command_lines):
        """
            locally run command lines

        :param command_lines: list of command lines
        :param filename:
        :return:
        """
        cmd = ";".join(command_lines)
        p = subprocess.Popen(cmd, shell=True, stderr=subprocess.STDOUT)
        p.communicate()
        p.wait()
        return p




class TraceParser(object):
    """
        a handler for a /tmp/trace file


        sample trace content:

        >-[ 2016-06-23T11:26:42Z dump_trace
        >-- 2016-06-23T11:24:54Z start_trace
        >-> 2016-06-23T11:24:54Z ps
          PID TTY           TIME CMD
        20886 ttys000    0:00.03 /opt/homebrew-cask/Caskroom/iterm2/2.1.4/iTerm.app/Contents/MacOS/iTerm2 --server login -fp cocoon
        20888 ttys000    0:00.02 -bash
        18771 ttys001    0:00.03 /opt/homebrew-cask/Caskroom/iterm2/2.1.4/iTerm.app/Contents/MacOS/iTerm2 --server login -fp cocoon
        18773 ttys001    0:00.05 -bash
        >-> 2016-06-23T11:26:39Z ps
          PID TTY           TIME CMD
        20886 ttys000    0:00.03 /opt/homebrew-cask/Caskroom/iterm2/2.1.4/iTerm.app/Contents/MacOS/iTerm2 --server login -fp cocoon
        20888 ttys000    0:00.02 -bash
        18771 ttys001    0:00.03 /opt/homebrew-cask/Caskroom/iterm2/2.1.4/iTerm.app/Contents/MacOS/iTerm2 --server login -fp cocoon
        18773 ttys001    0:00.05 -bash
        >-] 2016-06-23T11:26:42Z dump_trace


    """
    def __init__(self,content,device="device",configfile='collectors.ini',config=None):
        """

        :param content: list (list of lines)
        """
        self.content=content
        self.device=device

        self.config= config or {}
        self.configfile= configfile

        self._commands = []

        #self._handlers= TraceHandlers.handlers


        self._parse()
        return


    @classmethod
    def from_file(cls,filename='/tmp/trace',device='device',**kwargs):
        """
        :param filename:
        :return:
        """
        with open(filename,'r') as fh:
            content= fh.readlines()
            return cls(content,device,**kwargs)


    @classmethod
    def get_graphite_collector(self, collector_name,configfile=None,config=None):
        """

        :param collector_name: eg
        :return:
        """
        collector= catalog.get_graphite_collector(collector_name, config=config, configfile=configfile)
        return collector


    def _parse(self):
        """


        :return:
        """
        self._commands=[]
        current_command= None
        current_body =[]

        for line in self.content:

            # looking for >-> prefix
            if line.startswith(">--"):
                # comment
                continue
            elif line.startswith(">-["):
                # start block
                continue
            elif line.startswith(">-]"):
                # stop block
                continue

            if line.startswith(">-> ") or line.startswith(">-<"):
                # this is a command header or a command footer

                if current_command:

                    # save the last command
                    timestamp,name= self._parse_header(current_command)
                    entry= [ timestamp, name, current_body ]
                    self._commands.append(entry)

                else:
                    # first command
                    pass

                if line.startswith('>-<'):
                    # end command
                    current_command= None
                else:
                    # init
                    current_command= line

                current_body =[]

            else:
                # a line of command body
                if current_command:
                    current_body.append(line.strip())
                else:
                    # body outside a command
                    pass

        # no more lines dump last command if exists
        if current_command:
            # save the last command
            timestamp, name = self._parse_header(current_command)
            entry = [timestamp, name, current_body]
            self._commands.append(entry)

        # heapify commands
        heapq.heapify(self._commands)
        return self._commands


    def _parse_header(self,line):
        """


        :param line: string , eg  '>-> 2016-06-22T16:57:42Z ps
'
        :return: tuple  timestamp, name , comment
        """
        parts= line.split(" ")
        return parts[1].strip(), parts[2].strip()



    def __iter__(self):
        """

        :param filter:
        :return:
        """
        for line in self._commands:
            #      timestamp   command  , trace
            yield line[0] ,    line[1] , line[2]



    def push_to_graphite(self,timestamp,command,trace):
        """

        :param timestamp:
        :param command:
        :param trace:
        :return:
        """
        timestamp= TimestampConverter.iso_to_epoq(timestamp)
        collector= self.get_graphite_collector(command,configfile=self.configfile,config=self.config)

        r= collector.collect_metric(trace, timestamp=timestamp)

        return r


    def push_all_to_graphite(self):
        """

        :return:
        """
        for timestamp, metric_name, metric_lines in self:
            self.push_to_graphite(timestamp, metric_name, metric_lines)

        return
#
#
#
# class TraceHandler(object):
#     """
#
#     """
#     name = 'handler'
#     qualifier = 'metric'
#     cmd = 'sleep 1'
#
#
#     @classmethod
#     def register(cls):
#         """
#             self register to the parser
#
#         :param handler: TraceHandler
#         :return:
#         """
#         TraceHandlers.register_handler(cls.name,cls)
#
#     @classmethod
#     def timestamp(cls,timestamp):
#         """
#             convert timestamp to epoq
#
#         :param timestamp:
#         :return:
#         """
#         #  '2016-08-10T10:57:58Z' does not match format '%Y%m%dT%H%M%S.%fZ'
#
#         t= datetime.datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%SZ")
#         timestamp= t.strftime('%s')
#         return timestamp
#
#
#     def decode(self, timestamp, trace):
#         """
#
#         from an entry extract a list of tuple (timestamp,value)
#
#
#         :param entry: list : timestamp , name , lines
#         :return:
#         """
#         # return [ (metric,value,timestamp),...]
#         raise NotImplementedError
#
#     def trace_to_table(self,table_lines,sep=' '):
#         """
#
#         transform a list of lines , where the first line contains labels and others contains data
#
#             eg
#
#             label1 label2
#             v1      v2
#             v3      v4
#
#             =>  fields: [ { label1:v1, label2:v2 } , { label1:v3 ,label2:v4} ]
#
#         :param lines: a list of lines representing a table
#         :return: table or None
#         """
#         table= []
#
#         labels = []
#         try:
#             for line in trace:
#                 print line
#                 if not labels:
#                     labels = line.split(sep)
#                 else:
#                     fields = {}
#                     parts = line.split(sep)
#                     if len(parts) > len(labels):
#                         log.error('bad table format')
#                         table=None
#                         break
#                     for index, part in enumerate(parts):
#                         fields[labels[index]] = part.strip()
#                     table.append(fields)
#         except Exception ,e:
#             log.error('bad table forrmat (%s)' % str(e))
#             table=None
#         return table


if __name__=="__main__":

    import logging
    logging.basicConfig(level=logging.DEBUG)

    #
    # generate df metric to /tmp/trace
    #
    driver= TraceDriver(filename='/tmp/trace')


    begin_commands = driver.gen_start_trace()
    shell_cmd= driver.get_collector_command('df')
    df_commands = driver.gen_command_lines('df', shell_cmd)

    print begin_commands
    print df_commands

    driver.run_local(begin_commands)
    driver.run_local(df_commands)

    #
    # parse trace from /tmp/trace and inject into graphite
    #
    parser= TraceParser.from_file('/tmp/trace',device='tv',configfile='./collectors.ini')
    parser.push_all_to_graphite()



    print "Done."