
import sys
sys.path.append('..')

from restop_graph.catalog import get_graphite_collector, get_collector_command


def test_cpu_command_line():
    assert  get_collector_command('cpu') == "cat /proc/stat | grep cpu"

def test_loadavg_command_line():
    assert  get_collector_command('loadavg') == "cat /proc/loadavg"


def test_memory_command_line():
    assert  get_collector_command('memory') == "cat /proc/meminfo"