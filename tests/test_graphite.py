"""


    needs an instance of graphite vm running on localhost:22003



"""
import datetime

import sys
sys.path.append('..')

from restop_graph.catalog import get_graphite_collector


import logging
log= logging.getLogger(__name__)

logging.basicConfig(level=logging.DEBUG)



options= dict(host='127.0.0.1',port='2003',timeout=5,path_prefix='platform-1')





lines="""\
cpu  31 0 94 5773088 0 0 0 0 0 0
cpu0 31 0 94 5773088 0 0 0 0 0 0
intr 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
ctxt 0
btime 1466693266
processes 0
procs_running 0
procs_blocked 0
softirq 0 0 0 0 0 0 0 0 0 0 0
"""
cpu_lines=lines.split('\n')


lines="""\
0.00 0.01 0.02 3/59 1985
"""
loadavg_lines=lines.split('\n')


lines="""\
MemTotal:         507012 kB
MemFree:          475340 kB
MemAvailable:     484088 kB
Buffers:            3392 kB
Cached:            11308 kB
SwapCached:            0 kB
Active:            12084 kB
Inactive:           4268 kB
Active(anon):       1696 kB
Inactive(anon):      384 kB
Active(file):      10388 kB
Inactive(file):     3884 kB
Unevictable:           0 kB
Mlocked:               0 kB
SwapTotal:             0 kB
SwapFree:              0 kB
Dirty:                 0 kB
Writeback:             0 kB
AnonPages:          1708 kB
Mapped:             3728 kB
Shmem:               428 kB
Slab:               9180 kB
SReclaimable:       3144 kB
SUnreclaim:         6036 kB
KernelStack:         928 kB
PageTables:          464 kB
NFS_Unstable:          0 kB
Bounce:                0 kB
WritebackTmp:          0 kB
CommitLimit:      253504 kB
Committed_AS:       6792 kB
VmallocTotal:   34359738367 kB
VmallocUsed:        3400 kB
VmallocChunk:   34359685948 kB
DirectMap4k:        8128 kB
DirectMap2M:      516096 kB
"""

memory_lines=lines.split('\n')



timestamp= datetime.datetime.now().strftime('%s')



def test_cpu():

    col= get_graphite_collector('cpu', config=options,configfile='collectors.ini')
    r= col.collect_metric(cpu_lines,timestamp= timestamp)


def test_loadavg():

    col= get_graphite_collector('loadavg', config=options,configfile='collectors.ini')
    r= col.collect_metric(loadavg_lines,timestamp= timestamp)

def test_memory():

    col= get_graphite_collector('memory', config=options,configfile='collectors.ini')
    r= col.collect_metric(memory_lines,timestamp= timestamp)

    return r

if __name__=='__main__':

    #test_cpu()
    #test_loadavg()
    test_memory()

    print "Done"


