import datetime
import time

from restop_graph.busybox_stats import TraceDriver,TraceParser
from restop_graph.catalog import get_collector_command,get_graphite_collector

options= dict(host='127.0.0.1',port='2003',timeout=5,path_prefix='platform-1')


df_sample="""\
Filesystem     1K-blocks   Used Available Use% Mounted on
devtmpfs           10240      0     10240   0% /dev
shm               253504      0    253504   0% /dev/shm
/dev/sda3        7026040 375056   6271032   6% /
tmpfs              50704    120     50584   1% /run
cgroup_root        10240      0     10240   0% /sys/fs/cgroup
/dev/sda1          95054  30245     57641  35% /boot
"""





# get the basic shell command for metric
shell_cmd= get_collector_command('cpu')



# prepare command to inject
trace_driver= TraceDriver(filename='/tmp/trace')


begin_commands= trace_driver.gen_start_trace()
cpu_commands= trace_driver.gen_command_lines('cpu', shell_cmd)

print begin_commands
print cpu_commands


shell_cmd= get_collector_command('df')

df_commands= trace_driver.gen_command_lines('df', shell_cmd)
print df_commands

trace_driver.run_local(begin_commands)
trace_driver.run_local(df_commands)

#
# exploit stat result
results= file('/tmp/trace').readlines()
parser= TraceParser.from_file('/tmp/trace',device= 'tv')


for timestamp,metric_name,metric_lines in parser:
    parser.push_to_graphite(timestamp,metric_name,metric_lines)


now= datetime.datetime.now()
timestamp = time.mktime(now.timetuple())


col = get_graphite_collector('df', config=options, configfile='collectors.ini')

df_lines=df_sample.split('\n')
r = col.collect_metric(df_lines, timestamp=timestamp)


print "Done"